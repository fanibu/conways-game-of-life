import tkinter as tk

from gui.game_settings import GameSettingsPage
from gui.menu import MenuPage
from gui.objects_pages import StillObjectsPage, OscillatingObjectPage, SpaceshipsPage
from gui.special_pattern_window import PatternPage
from gui.tutorial import TutorialPage

class UserGUI(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.title("Conway's Game of Life")
        self.all_pages_list = [MenuPage, TutorialPage, GameSettingsPage, PatternPage, StillObjectsPage,
                               OscillatingObjectPage, SpaceshipsPage]
        self.current_page = None
        self.pages = {}
        for Page in self.all_pages_list:
            frame = Page(self)
            self.pages[Page.__name__] = frame

        self.show_page("MenuPage")

    def show_page(self, page: str):
        if self.current_page is not None:
            self.current_page.pack_forget()

        frame = self.pages[page]
        frame.pack()
        self.current_page = frame
        self.center_window()

    def center_window(self):
        self.update_idletasks()
        self.update()

        window_width = self.winfo_width()
        window_height = self.winfo_height()

        x = (self.winfo_screenwidth() - window_width) / 2
        y = (self.winfo_screenheight() - window_height) / 2
        print(window_width, window_height)

        self.geometry(f"{window_width}x{window_height}+{int(x)}+{int(y)}")
        self.geometry("")  # reset location and size, otherwise geometry manager is unable to pack()


if __name__ == '__main__':
    gui = UserGUI()
    gui.mainloop()
