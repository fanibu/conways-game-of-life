import tkinter as tk
from tkinter import font as tkFont

from PIL import Image, ImageTk

from gui.page import Page


class TutorialPage(Page):
    BG = "white"

    def __init__(self, parent):
        Page.__init__(self, parent, bg=TutorialPage.BG)

        self.canvas = tk.Canvas(self, bg=TutorialPage.BG)
        self.scrollbar = tk.Scrollbar(self, command=self.canvas.yview)
        self.scrollframe = tk.Frame(self.canvas, bg=TutorialPage.BG)

        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.canvas.pack(side=tk.LEFT)
        self.canvas.create_window((0,0), window=self.scrollframe, anchor="nw")
        self.scrollframe.bind("<Configure>", self.onScrollWindowConfigure)

        self.add_gui_elements()

    def add_gui_elements(self):

        exit_button = tk.Button(self.scrollframe, text="Zurück", font=("Arial",15), command=lambda : self.parent.show_page("MenuPage"))
        exit_button.grid(row=0, column=0, padx=(10,20), pady=10)

        titel_font = tkFont.Font(family="Helvetica", size=25, weight="bold", underline=True)
        titel = tk.Label(self.scrollframe, text="Spielregeln", font=titel_font, bg=TutorialPage.BG, fg="black")
        titel.grid(row=0, column=1, columnspan=10, pady=20)

        normal_text_font = tkFont.Font(family="sans-serif", size=12)
        cell_state_lb = tk.Label(self.scrollframe, text="Das Spielfeld ist ein im Idealfall unendlich großes Gitter. Jedes Gitterquadrat ist eine Zelle,\n"
                                            "die entweder den Zustand tot oder den Zustand lebendig einnehmen kann.\n\n"
                                            "Zunächst wird eine Anfangsgeneration von lebenden und toten Zellen auf dem Spielfeld platziert.\n"
                                            "Jede Zelle hat auf diesem Spielfeld genau acht Nachbarzellen.\n\n"
                                            "Die Folgegeneration wird für alle Zellen gleichzeitig berechnet und hängt nur vom aktuellen\n"
                                            "Zustand der Zelle selbst und den aktuellen Zuständen ihrer acht Nachbarzellen ab.",
                                    font=normal_text_font, justify=tk.LEFT, bg=TutorialPage.BG)
        cell_state_lb.grid(row=1, column=1, columnspan=4, pady=(0,40), padx=(0,20))

        rule_amount_font = tkFont.Font(family="Arial", size=12)
        rule_1_lb = tk.Label(self.scrollframe, text="0-1\n lebende Nachbarn", font=rule_amount_font, bg=TutorialPage.BG)
        rule_1_lb.grid(row=3, column=1)

        rule_2_lb = tk.Label(self.scrollframe, text="4-8\n lebende Nachbarn", font=rule_amount_font, bg=TutorialPage.BG)
        rule_2_lb.grid(row=3, column=2)

        rule_3_lb = tk.Label(self.scrollframe, text="2-3\n lebende Nachbarn", font=rule_amount_font, bg=TutorialPage.BG)
        rule_3_lb.grid(row=3, column=3)

        rule_4_lb = tk.Label(self.scrollframe, text="genau 3\n lebende Nachbarn", font=rule_amount_font, bg=TutorialPage.BG)
        rule_4_lb.grid(row=3, column=4)

        rules_img = Image.open("../res/rules_2.png")
        """basewidth = 1000
        wpercent = (basewidth / float(rules_img.size[0]))
        hsize = int((float(rules_img.size[1]) * float(wpercent)))
        rules_img = rules_img.resize((basewidth, hsize), Image.ANTIALIAS)"""
        rules_img = ImageTk.PhotoImage(rules_img)
        rules_img_lb = tk.Label(self.scrollframe, image=rules_img, borderwidth=0)
        rules_img_lb.image = rules_img #to prevent garbage collection
        rules_img_lb.grid(row=4, column=1, columnspan=5, pady=(10,0))

        border_rules_caption_font = tkFont.Font(family="Helvetica", size=20, weight="bold", underline=True)
        border_rules_caption_lb = tk.Label(self.scrollframe, text="Randbedingungen", font=border_rules_caption_font, bg=TutorialPage.BG)
        border_rules_caption_lb.grid(row=5, column=1, columnspan=10, pady=(20,20))

        border_rules_lb = tk.Label(self.scrollframe, text="Am Rand des Spielfelds gibt es zwei verschiedene Regelungen.\n\n"
                                   "1. Torodiale Topologie: Gegenüberliegende Kanten des Gitters sind miteinander"
                                    " verbunden.\n    Damit wird ein ungehinderter Transport von Zellstatusinformationen"
                                    " \n    über die Gittergrenzen hinweg gewährleistet. (siehe Bild links). \n\n"
                                   "2. Zellen außerhalb des Gitters werden behandelt, als wären sie tot. Damit kann "
                                    "verhindert werden,\n    dass sich fortbewegende Objekte wieder an der\n    gegenüberliegenden Kante"
                                   " in das Spielfeld eintreten. (siehe Bild rechts)",
                                   font=normal_text_font, justify=tk.LEFT, bg=TutorialPage.BG)
        border_rules_lb.grid(row=6, column=1, columnspan=4)

        border_rules_img = ImageTk.PhotoImage(Image.open("../res/border_rules.png"))
        border_rules = tk.Label(self.scrollframe, image=border_rules_img, borderwidth=0)
        border_rules.image = border_rules_img
        border_rules.grid(row=6, column=5, columnspan=6, pady=20)


    def onScrollWindowConfigure(self, event):
        self.canvas.config(scrollregion=self.canvas.bbox("all"), width=1500, height=800)




