import tkinter as tk

from PIL import Image, ImageTk

from gui.page import Page


class PatternPage(Page):
    def __init__(self, parent):
        super().__init__(parent, self.BG)
        self.add_gui_elements()

    def add_gui_elements(self):
        self.add_exit_button()
        self.add_headline()
        self.add_intro_text()
        self.add_buttons()

    def add_headline(self):
        titel = tk.Label(self, text="Besondere Objekte", font=self.headline_font, bg=self.BG, fg="black")
        titel.grid(row=0, column=0, pady=20)

    def add_intro_text(self):
        explanation_text = "Im Laufe des Spieles entstehen auf dem \nSpielfeld mehrere komplexe Struckturen. \nTypische " \
                           "Objekte, die dabei enstehen, \nlassen sich auf Grund ihrer Eigenschaften \nin Klassen " \
                           "einteilen. "
        explanation_lable = tk.Label(self, text=explanation_text, font=self.text_font, justify=tk.LEFT,
                                     bg=self.BG)
        explanation_lable.grid(row=1, column=0, pady=(0, 20))

    def add_info_static_objects(self):
        subtitle = tk.Label(self, text="Statische Objekte", font=self.subheading_font, bg=self.BG, fg="black")
        subtitle.grid(row=2, column=0)
        info_text = "Diese Objektklasse verändert im Laufe " \
                    "\ndes Spieles, ohne äußere Einflüsse, nicht ihre From.\n" \
                    "Sie bilden somit ein stabiles Zellensystem."
        info_text_lable = tk.Label(self, text=info_text, font=self.text_font, justify=tk.LEFT, bg=self.BG)
        info_text_lable.grid(row=3, column=0, pady=(0, 20), padx=(0, 20))

        self.add_boat_class()

    def add_boat_class(self):
        headline = tk.Label(self, text="Boot", font=self.classheading_font, bg=self.BG, fg="black")
        headline.grid(row=4, column=0)
        block_img = ImageTk.PhotoImage(Image.open("imgs/boat.png"))
        block = tk.Label(self, image=block_img, borderwidth=0)
        block.image = block_img
        block.grid(row=5, column=0)

    def add_buttons(self):
        still_objects_button = tk.Button(self, text="Statische Objekte", font=self.text_font,
                                         command=lambda: self.parent.show_page("StillObjectsPage"))
        still_objects_button.grid(row=2, column=0, pady=(10, 10))
        still_objects_button = tk.Button(self, text="Oszillierende Objekte", font=self.text_font,
                                         command=lambda: self.parent.show_page("OscillatingObjectPage"))
        still_objects_button.grid(row=3, column=0, pady=(10, 10))
        still_objects_button = tk.Button(self, text="Raumschiff Objekte", font=self.text_font,
                                         command=lambda: self.parent.show_page("SpaceshipsPage"))
        still_objects_button.grid(row=4, column=0, pady=(10, 20))

    def add_exit_button(self):
        exit_button = tk.Button(self, text="Zurück", font=("Arial", 15),
                                command=lambda: self.parent.show_page("MenuPage"))
        exit_button.grid(row=5, column=0, padx=(5, 20), sticky=tk.W, pady=(20, 5))
