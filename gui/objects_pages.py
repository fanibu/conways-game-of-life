import tkinter as tk

from PIL import ImageTk, Image

from gui.page import Page


class FrameGenerator:
    def __init__(self, frames):
        self.frames = frames
        self.pointer = 0

    def __next__(self):
        self.pointer += 1
        if self.pointer >= len(self.frames):
            self.pointer = 0
            return self.frames[self.pointer]
        return self.frames[self.pointer]


class StillObjectsPage(Page):
    def __init__(self, parent):
        super().__init__(parent, StillObjectsPage.BG)
        self.add_gui_elements()

    def add_gui_elements(self):
        self.add_headline()
        self.add_info_text()
        self.add_examples()

        self.add_exit_button()

    def add_headline(self):
        subtitle = tk.Label(self, text="Statische Objekte", font=self.subheading_font, bg=self.BG,
                            fg="black")
        subtitle.grid(row=0, column=0, columnspan=3)

    def add_info_text(self):
        info_text = "Diese Objektklasse verändert im Laufe " \
                    "\ndes Spieles, ohne äußere Einflüsse, nicht ihre From.\n" \
                    "Sie bilden somit ein stabiles Zellensystem."
        info_text_lable = tk.Label(self, text=info_text, font=self.text_font, justify=tk.CENTER, bg=self.BG)
        info_text_lable.grid(row=1, column=0, pady=(0, 20), padx=(0, 20), columnspan=3)

    def add_examples(self):
        self.add_boat()
        self.add_loaf()
        self.add_beehive()

    def add_boat(self):
        headline = tk.Label(self, text="Boot", font=self.classheading_font, bg=self.BG, fg="black")
        headline.grid(row=2, column=0)

        block_img = ImageTk.PhotoImage(Image.open("../res/imgs/still/boat.png"))
        block = tk.Label(self, image=block_img, borderwidth=0)
        block.image = block_img
        block.grid(row=3, column=0)

    def add_loaf(self):
        headline = tk.Label(self, text="Brot", font=self.classheading_font, bg=self.BG, fg="black")
        headline.grid(row=2, column=1)

        block_img = ImageTk.PhotoImage(Image.open("../res/imgs/still/loaf.png"))
        block = tk.Label(self, image=block_img, borderwidth=0)
        block.image = block_img
        block.grid(row=3, column=1)

    def add_beehive(self):
        headline = tk.Label(self, text="Bienenstock", font=self.classheading_font, bg=self.BG, fg="black")
        headline.grid(row=2, column=2)

        block_img = ImageTk.PhotoImage(Image.open("../res/imgs/still/beehive.png"))
        block = tk.Label(self, image=block_img, borderwidth=0)
        block.image = block_img
        block.grid(row=3, column=2)

    def add_exit_button(self):
        exit_button = tk.Button(self, text="Zurück", font=("Arial", 15),
                                command=lambda: self.parent.show_page("PatternPage"))
        exit_button.grid(row=4, column=0, padx=(5, 20), columnspan=3, pady=(20, 5), sticky=tk.W)


class OscillatingObjectPage(Page):
    def __init__(self, parent):
        super().__init__(parent, OscillatingObjectPage.BG)

    def add_gui_elements(self):
        self.add_headline()
        self.add_info_text()
        self.add_examples()

        self.add_exit_button()
        self.after(1000, self.update_gifs)

    def add_headline(self):
        subtitle = tk.Label(self, text="Oszillierende Objekte", font=self.subheading_font, bg=self.BG,
                            fg="black")
        subtitle.grid(row=0, column=0, columnspan=3)

    def add_info_text(self):
        info_text = "Diese Objektklasse verändert ihre From periodisch.\n" \
                    "Das bedeutet nach einer gewissen Anzahl an Spiel Iterationen \nwiederholt sich die From "
        info_text_lable = tk.Label(self, text=info_text, font=self.text_font, justify=tk.CENTER, bg=self.BG)
        info_text_lable.grid(row=1, column=0, pady=(0, 20), padx=(0, 20), columnspan=3)

    def add_examples(self):
        self.add_bacon()
        self.add_blinker()
        self.add_toad()

    def add_bacon(self):
        headline = tk.Label(self, text="Bacon", font=self.classheading_font, bg=self.BG, fg="black")
        headline.grid(row=2, column=0)

        bacon_gif_images = []
        imagename = "../res/imgs/oscillators/bacon.gif"
        for i in range(2):
            bacon_gif_images.append(tk.PhotoImage(file=imagename, format=f"gif -index {i}"))

        self.bacon_generator = FrameGenerator(bacon_gif_images)

        self.bacon = tk.Label(self, image=bacon_gif_images[0], borderwidth=0)
        self.bacon.grid(row=3, column=0)

    def add_blinker(self):
        headline = tk.Label(self, text="Blinker", font=self.classheading_font, bg=self.BG, fg="black")
        headline.grid(row=2, column=1)

        blinker_gif_images = []
        imagename = "../res/imgs/oscillators/blinker.gif"

        for i in range(2):
            blinker_gif_images.append(tk.PhotoImage(file=imagename, format=f"gif -index {i}"))

        self.blinker_generator = FrameGenerator(blinker_gif_images)

        self.blinker = tk.Label(self, image=blinker_gif_images[0], borderwidth=0)
        self.blinker.grid(row=3, column=1)

    def add_toad(self):
        headline = tk.Label(self, text="Toad", font=self.classheading_font, bg=self.BG, fg="black")
        headline.grid(row=2, column=2)

        toad_gif_images = []
        imagename = "../res/imgs/oscillators/toad.gif"

        for i in range(2):
            toad_gif_images.append(tk.PhotoImage(file=imagename, format=f"gif -index {i}"))

        self.toad_generator = FrameGenerator(toad_gif_images)

        self.toad = tk.Label(self, image=toad_gif_images[0], borderwidth=0)
        self.toad.grid(row=3, column=2)

    def add_exit_button(self):
        exit_button = tk.Button(self, text="Zurück", font=("Arial", 15),
                                command=lambda: self.parent.show_page("PatternPage"))
        exit_button.grid(row=4, column=0, padx=(5, 20), columnspan=3, pady=(20, 5), sticky=tk.W)

    def update_gifs(self):
        self.bacon.configure(image=self.bacon_generator.__next__())
        self.blinker.configure(image=self.blinker_generator.__next__())
        self.toad.configure(image=self.toad_generator.__next__())
        self.after(500, self.update_gifs)


class SpaceshipsPage(Page):
    def __init__(self, parent):
        super().__init__(parent, SpaceshipsPage.BG)

    def add_gui_elements(self):
        self.add_headline()
        self.add_info_text()
        self.add_examples()

        self.add_exit_button()

        self.after(35, self.update_gifs)

    def add_headline(self):
        subtitle = tk.Label(self, text=" Raumschiffe", font=self.subheading_font, bg=self.BG,
                            fg="black")
        subtitle.grid(row=0, column=0, columnspan=3)

    def add_info_text(self):
        info_text = "Wie der Name schon verrät, können diese Objekte " \
                    "über die Spielfläche fliegen."
        info_text_lable = tk.Label(self, text=info_text, font=self.text_font, justify=tk.CENTER, bg=self.BG)
        info_text_lable.grid(row=1, column=0, pady=(0, 20), padx=(0, 20), columnspan=3)

    def add_examples(self):
        self.add_glider()
        self.add_light_spaceship()
        self.add_heavy_spaceship()

    def add_glider(self):
        headline = tk.Label(self, text="Gleiter", font=self.classheading_font, bg=self.BG, fg="black")
        headline.grid(row=2, column=0)

        glider_gif_images = []
        imagename = "../res/imgs/spaceships/glider.gif"

        for i in range(14):
            glider_gif_images.append(tk.PhotoImage(file=imagename, format=f"gif -index {i}"))

        self.glider_generator = FrameGenerator(glider_gif_images)

        self.glider = tk.Label(self, image=glider_gif_images[0], borderwidth=0)
        self.glider.grid(row=3, column=0)

    def add_light_spaceship(self):
        headline = tk.Label(self, text="Leichtes \nRaumschiff", font=self.classheading_font, bg=self.BG, fg="black")
        headline.grid(row=2, column=1)

        light_spaceship_gif_images = []
        imagename = "../res/imgs/spaceships/light_spaceship.gif"

        for i in range(28):
            light_spaceship_gif_images.append(tk.PhotoImage(file=imagename, format=f"gif -index {i}"))

        self.light_spaceship_generator = FrameGenerator(light_spaceship_gif_images)

        self.light_spaceship = tk.Label(self, image=light_spaceship_gif_images[0], borderwidth=0)
        self.light_spaceship.grid(row=3, column=1)

    def add_heavy_spaceship(self):
        headline = tk.Label(self, text="Schweres \nRaumschiff", font=self.classheading_font, bg=self.BG, fg="black")
        headline.grid(row=2, column=2)

        heavy_spaceship_gif_images = []
        imagename = "../res/imgs/spaceships/heavy_spaceship.gif"

        for i in range(28):
            heavy_spaceship_gif_images.append(tk.PhotoImage(file=imagename, format=f"gif -index {i}"))

        self.heavy_spaceship_generator = FrameGenerator(heavy_spaceship_gif_images)

        self.heavy_spaceship = tk.Label(self, image=heavy_spaceship_gif_images[0], borderwidth=0)
        self.heavy_spaceship.grid(row=3, column=2)

    def add_exit_button(self):
        exit_button = tk.Button(self, text="Zurück", font=("Arial", 15),
                                command=lambda: self.parent.show_page("PatternPage"))
        exit_button.grid(row=4, column=0, padx=(5, 20), columnspan=3, pady=(20, 5), sticky=tk.W)

    def update_gifs(self):
        self.glider.configure(image=self.glider_generator.__next__())
        self.light_spaceship.configure(image=self.light_spaceship_generator.__next__())
        self.heavy_spaceship.configure(image=self.heavy_spaceship_generator.__next__())
        self.after(35, self.update_gifs)
