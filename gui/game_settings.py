import tkinter as tk
from tkinter import font
from tkinter import messagebox

from game.rle_reader import RleReader
from game.simulation import Simulation
from gui.page import Page


class GameSettingsPage(Page):

    BG = "grey"

    def __init__(self, parent):
        self.random_generation = tk.IntVar()
        self.random_probability = tk.IntVar()
        self.fps = tk.IntVar()
        self.cell_size = tk.IntVar()
        self.window_size = tk.IntVar()
        self.fullscreen = tk.IntVar()
        self.mirrorfield = tk.IntVar()

        self.structure_type_lists = {}

        self.window_size_scale_max = 100
        self.max_fps = 100

        Page.__init__(self, parent, bg=GameSettingsPage.BG)
        self.add_gui_elements()

        self.screen_ratio = self.winfo_screenwidth() / self.winfo_screenheight()
        self.game = None

    def add_gui_elements(self):
        exit_button = tk.Button(self, text="Zurück", font=("Arial", 15),
                                command=lambda: self.parent.show_page("MenuPage"))
        exit_button.grid(row=0, column=0, padx=(10, 20), pady=10)

        subcaption_font = font.Font(family="Helvetica", size=14, underline=True)

        self.add_structure_type_gui_elements(subcaption_font)
        self.add_game_settings_gui_elements(subcaption_font)

        start_game_button = tk.Button(self, text="Spiel starten", font=("Arial", 15), command=self.start_game_click)
        start_game_button.grid(row=8, column=1, columnspan=2, pady=20)


    def add_structure_type_gui_elements(self, subcaption_font : font.Font):
        strutctures_font = font.Font(family="Helvetica", size=20, weight="bold", underline=False)
        structures_lb = tk.Label(self, text="Spielstrukturen", font=strutctures_font,
                                 bg=GameSettingsPage.BG)
        structures_lb.grid(row=0, column=1, padx=(0, 20), pady=20)

        structure_types_frame = tk.Frame(self, bg=GameSettingsPage.BG)
        all_structure_types = RleReader.read_all_rle_file_names()

        position_counter = 0
        for structure_type_name in all_structure_types.keys():
            structure_type_caption = tk.Label(structure_types_frame, text=structure_type_name, font=subcaption_font, bg=GameSettingsPage.BG)
            structure_type_caption.grid(row=int(position_counter/2) * 2, column=position_counter % 2)

            structure_type_list = tk.Listbox(structure_types_frame, height=5)
            for structure_type in all_structure_types.get(structure_type_name):
                structure_type_list.insert(tk.END, structure_type)
            structure_type_list.grid(row=int(position_counter/2) * 2 + 1, column=position_counter % 2, padx=10)

            self.structure_type_lists[structure_type_name] = structure_type_list
            position_counter += 1

        post_structure_types_position = int(position_counter/2) * 2 + 2

        random_lb = tk.Label(structure_types_frame, text="Lebenswahrscheinlichkeit:", font=("Arial", 10, "bold"), bg=GameSettingsPage.BG)

        random_probability_scale = tk.Scale(structure_types_frame, variable=self.random_probability, from_=0, to=100, orient=tk.HORIZONTAL,
                                            length=100, highlightbackground=GameSettingsPage.BG, bg=GameSettingsPage.BG)
        random_probability_scale.set(50)

        random_checkbox = tk.Checkbutton(structure_types_frame, variable=self.random_generation, text="Zufällige Anfangsgeneration",
                                         command=lambda: self.random_generation_click(post_structure_types_position, random_lb, random_probability_scale),
                                         activebackground=GameSettingsPage.BG, bg=GameSettingsPage.BG)
        random_checkbox.grid(row=post_structure_types_position, column=0, columnspan=2, pady=(30,5))

        structure_types_frame.grid(row=1, column=1, padx=10)

    def random_generation_click(self, post_structure_types_position: int, random_lb : tk.Label, random_probability_scale: tk.Scale):
        if self.random_generation.get():
            random_lb.grid(row=post_structure_types_position+1, column=0, sticky=tk.S)
            random_probability_scale.grid(row=post_structure_types_position+1, column=1)
        else:
            random_lb.grid_forget()
            random_probability_scale.grid_forget()

    def add_game_settings_gui_elements(self, subcaption_font : font.Font):
        settings_title_font = font.Font(family="Helvetica", size=20, weight="bold", underline=False)
        settings_lb = tk.Label(self, text="Spieloptionen", font=settings_title_font, bg=GameSettingsPage.BG)
        settings_lb.grid(row=0, column=2, padx=20)

        game_settings_frame = tk.Frame(self, bg=GameSettingsPage.BG)

        fps_lb = tk.Label(game_settings_frame, text="Speed", font=subcaption_font, bg=GameSettingsPage.BG)
        fps_lb.grid(row=0, column=0, pady=(10,0))

        fps_scale = tk.Scale(game_settings_frame, variable=self.fps, from_=0, to=self.max_fps, orient=tk.HORIZONTAL, length=200,
                                    command=self.fps_changed,
                                    highlightbackground=GameSettingsPage.BG, bg=GameSettingsPage.BG)
        fps_scale.set(30)
        fps_scale.grid(row=1, column=0)

        cell_size_lb = tk.Label(game_settings_frame, text="Zellgröße", font=subcaption_font, bg=GameSettingsPage.BG)
        cell_size_lb.grid(row=2,  column=0, pady=(10,0))

        cell_size_scale = tk.Scale(game_settings_frame, variable=self.cell_size, from_=5, to=100, orient=tk.HORIZONTAL, length=200,
                                    highlightbackground=GameSettingsPage.BG, bg=GameSettingsPage.BG)
        cell_size_scale.set(20)
        cell_size_scale.grid(row=3,  column=0)

        window_size_lb = tk.Label(game_settings_frame, text="Fenstergröße", font=subcaption_font, bg=GameSettingsPage.BG)
        window_size_lb.grid(row=4,  column=0, pady=(10,0))

        window_size_scale = tk.Scale(game_settings_frame, variable=self.window_size, from_=0, to=self.window_size_scale_max, orient=tk.HORIZONTAL, length=200,
                                    highlightbackground=GameSettingsPage.BG, bg=GameSettingsPage.BG)
        window_size_scale.set(50)
        window_size_scale.grid(row=5,  column=0)

        fullscreen_checkbox = tk.Checkbutton(game_settings_frame, variable=self.fullscreen, text="Fullscreen",
                                    activebackground=GameSettingsPage.BG, bg=GameSettingsPage.BG)
        fullscreen_checkbox.grid(row=6,  column=0)

        border_rules_lb = tk.Label(game_settings_frame, text="Randbedingungen", font=subcaption_font, bg=GameSettingsPage.BG)
        border_rules_lb.grid(row=7, column=0, pady=(10,0))

        mirror_checkbox = tk.Checkbutton(game_settings_frame, variable=self.mirrorfield, text="Toroidale Welt",
                                         activebackground=GameSettingsPage.BG, bg=GameSettingsPage.BG)
        mirror_checkbox.grid(row=8,  column=0)

        game_settings_frame.grid(row=1, column=2, padx=10, sticky=tk.N)

    def calc_cell_dimensions(self):
        cell_size = self.cell_size.get()
        if self.fullscreen.get():
            horizontal_cells_range = int(self.winfo_screenwidth() / cell_size)
            vertical_cells_range = int(self.winfo_screenheight() / cell_size)
        else:
            cells_min_x = 14
            cells_max_x = int(self.winfo_screenwidth() / cell_size)

            horizontal_cells_range = cells_min_x + int((cells_max_x - cells_min_x) * (self.window_size.get() / self.window_size_scale_max))
            vertical_cells_range = int(horizontal_cells_range / self.screen_ratio)

        return horizontal_cells_range, vertical_cells_range, cell_size

    def check_if_cell_dimensions_are_compatible_with_structure_type(self, structure_type: str,
                                                                    horizontal_cells_range: int, vertical_cells_range: int,
                                                                    cell_size: int):
        additional_structure_cells = 16 # to give some extra space for the structures
        structure_columns, structure_rows = RleReader.get_structure_dimensions(structure_type)
        structure_columns += additional_structure_cells
        structure_rows += additional_structure_cells
        if horizontal_cells_range >= structure_columns and vertical_cells_range >= structure_rows: # fits in the window
            return horizontal_cells_range, vertical_cells_range, cell_size
        else:
            fullscreen = self.fullscreen.get()
            if fullscreen:
                window_space = 0
            else:
                window_space = 100 # minimum left space for screen border to window border if not fullscreen

            max_window_width = self.winfo_screenwidth() - window_space
            max_window_height = self.winfo_screenheight() - window_space
            relative_row_height = structure_rows * self.screen_ratio
            if relative_row_height > structure_columns and (structure_rows * cell_size) > max_window_height:
                cell_size = int(max_window_height / structure_rows)

            elif (structure_columns * cell_size) > max_window_width:
                cell_size = int(max_window_width / structure_columns)

            if fullscreen:
                structure_rows = int(max_window_height / cell_size)
                structure_columns = int(max_window_width / cell_size)

            return structure_columns, structure_rows, cell_size

    def fps_changed(self, new_fps: str):
        if self.game is None:
            return

        if int(new_fps) == 0:
            self.game.stop = True
        else:
            self.game.stop = False
            if new_fps == self.max_fps:
                self.game.wait_time = 0
            else:
                self.game.wait_time = 1 / int(new_fps)

    def start_game_click(self):
        if self.game is not None and self.game.running:
            self.game.running = False
            self.game.join()

        self.start_game()

    def get_structure_type_selection(self):
        for structure_type_name in self.structure_type_lists.keys():
            structure_type_list = self.structure_type_lists.get(structure_type_name)
            selected_structure_type_index = structure_type_list.curselection()
            if len(selected_structure_type_index) != 0:
                selected_structure_type_name = structure_type_name
                selected_structure_type = structure_type_list.get(selected_structure_type_index)
                return selected_structure_type_name + "/" + selected_structure_type

    def start_game(self):
        horizontal_cells_range, vertical_cells_range, cell_size = self.calc_cell_dimensions()

        if self.random_generation.get():

            self.game = Simulation("Zufällig", horizontal_cells_range, vertical_cells_range, self.fullscreen.get(),
                                cell_size, self.fps.get(), self.mirrorfield.get(), random_probability=self.random_probability.get())
        else:
            structure_type = self.get_structure_type_selection()
            if structure_type is None:
                messagebox.showerror("Selection Error", "Wähle entweder eine Spielstruktur aus, oder eine zufällige Anfangsgeneration")
                return

            horizontal_cells_range, vertical_cells_range, cell_size =\
                self.check_if_cell_dimensions_are_compatible_with_structure_type(structure_type, horizontal_cells_range,
                                                                                 vertical_cells_range, cell_size)

            self.game = Simulation(structure_type, horizontal_cells_range, vertical_cells_range, self.fullscreen.get(),
                                   cell_size, self.fps.get(), self.mirrorfield.get())

        self.game.start()






