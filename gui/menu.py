import tkinter as tk
from tkinter import font as tkFont

from gui.page import Page


class MenuPage(Page):
    BG = "grey"
    def __init__(self, parent):
        Page.__init__(self, parent, bg=MenuPage.BG)
        self.add_gui_elements()

    def add_gui_elements(self):
        titel_font = tkFont.Font(family="Times", size=40, weight="bold", slant="italic", underline=True)
        titel = tk.Label(self, text="Conway's Game of Life", font=titel_font, bg="grey", fg="lightgreen")
        titel.pack(pady=(15,70), padx=20)

        tutorial_btn = tk.Button(self, text="Tutorial", font=("Arial", 30), bg="lightyellow",
                                    command=self.tutorial_clicked)
        tutorial_btn.pack(pady=20)

        structures_btn = tk.Button(self, text="Strukturen", font=("Arial", 30), bg="lightyellow",
                                    command=self.structures_clicked)
        structures_btn.pack(pady=20)

        game_btn = tk.Button(self, text="Spieloptionen", font=("Arial", 30), bg="lightyellow",
                                    command=self.game_clicked)
        game_btn.pack(pady=20)

    def tutorial_clicked(self):
        self.parent.show_page("TutorialPage")

    def game_clicked(self):
        self.parent.show_page("GameSettingsPage")

    def structures_clicked(self):
        self.parent.show_page("PatternPage")

