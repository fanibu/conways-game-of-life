import abc
import tkinter as tk
from tkinter import font as tkFont


class Page(tk.Frame):
    BG = "gray"

    def __init__(self, parent, bg: str):
        tk.Frame.__init__(self, parent, bg=bg)

        self.parent = parent

        self.headline_font = tkFont.Font(family="Helvetica", size=30, weight="bold", underline=True)
        self.subheading_font = tkFont.Font(family="Helvetica", size=24, weight="bold")
        self.classheading_font = tkFont.Font(family="Helvetica", size=18, weight="bold")
        self.text_font = ("sans-serif", 15)

    @abc.abstractmethod
    def add_gui_elements(self):
        pass