import time
from threading import Thread

from game import game_gui_pygame
from game.cell import Cell
from game.rle_reader import RleReader


class Simulation(Thread):
    def __init__(self, game_type: str, horizontal_cells_range: int, vertical_cells_range: int, fullscreen: bool,
                 cell_size: int, fps: int, mirrorfield: bool, random_probability=-1):
        Thread.__init__(self)
        self.setDaemon(True) # to close pygame window, when tkinter gui is closed

        self.game_type = game_type
        self.cell_size = cell_size
        self.mirrorfield = mirrorfield
        self.random_probability = random_probability

        if fps != 0:
            self.wait_time = 1 / fps
            self.stop = False
        else:
            self.wait_time = 0
            self.stop = True

        self.horizontal_cells_range = horizontal_cells_range
        self.vertical_cells_range = vertical_cells_range

        self.cell_grid = []
        self.linear_cells = []

        window_width = horizontal_cells_range * cell_size
        window_height = vertical_cells_range * cell_size
        self.gui = game_gui_pygame.Game_GUI(window_width, window_height, fullscreen, cell_size)

        self.running = False

    def init(self):
        self.create_all_cells()
        if self.game_type != "Zufällig":
            RleReader.setup_gamefield(self.game_type, self.cell_grid, self.horizontal_cells_range, self.vertical_cells_range)

        self.gui.init(self.linear_cells)

    def create_all_cells(self):
        for y in range(self.vertical_cells_range):
            row = []
            for x in range(self.horizontal_cells_range):
                cell = Cell(self.cell_size, x, y, self.random_probability)
                row.append(cell)
                self.linear_cells.append(cell)

            self.cell_grid.append(row)

        self.init_find_neighbors()

    def init_find_neighbors(self):
        for cell in self.linear_cells:
            cell.find_neighbor_cells(self.mirrorfield, self.cell_grid,
                                     self.horizontal_cells_range, self.vertical_cells_range)

    def update_grid(self):
        for cell in self.linear_cells:
            cell.update_alive()

        self.gui.draw_all_cells(self.linear_cells)

        for cell in self.linear_cells:
            cell.alive = cell.next_round_alive

    def run(self):
        self.running = True
        self.init()

        while not self.gui.should_close() and self.running:
            time.sleep(self.wait_time)

            if not self.stop:
                self.update_grid()
                self.gui.update()

        self.gui.close()
        self.running = False