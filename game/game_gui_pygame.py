import pygame


class Game_GUI:
    window: pygame.Surface

    def __init__(self, width: int, height: int, fullscreen: bool, cell_size : int):
        self.window_width = width
        self.window_height = height
        self.fullscreen = fullscreen
        self.cell_size = cell_size

        self.background_color = (0,0,0) #black
        self.grid_color = (192,192,192) #grey
        self.alive_color = (51,255,51) #green

    def draw_cell_grid(self):
        pygame.draw.rect(self.window, self.background_color, (0,0, self.window_height, self.window_width))
        for x in range(self.cell_size, self.window_width, self.cell_size):
            pygame.draw.line(self.window, self.grid_color, (x, 0), (x, self.window_height))

        for y in range(self.cell_size, self.window_height, self.cell_size):
            pygame.draw.line(self.window, self.grid_color, (0, y), (self.window_width, y))

    def draw_all_cells(self, linear_cells: list):
        for cell in linear_cells:
            if cell.next_round_alive == cell.alive:  # no need to redraw
                continue

            if cell.next_round_alive:
                color = self.alive_color
            else:
                color = self.background_color

            pygame.draw.rect(self.window, color, (cell.draw_x1, cell.draw_y1, self.cell_size - 1, self.cell_size - 1))

    def init(self, linear_cells : list):
        pygame.init()
        if self.fullscreen:
            self.window = pygame.display.set_mode((0,0), pygame.FULLSCREEN)
        else:
            self.window = pygame.display.set_mode((self.window_width, self.window_height))
        pygame.display.set_caption("Conway's Game of Life")

        self.draw_cell_grid()
        self.draw_all_cells(linear_cells)
        self.update()

    def center_window(self):  #TODO
        pass
        """"x = (DISPLAY_WIDTH - self.window_width) / 2
        y = (DISPLAY_HEIGHT - self.window_height) / 2
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (x,y)"""""

    def update(self):
        pygame.display.update()

    def should_close(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return True
        return False

    def close(self):
        pygame.quit()
