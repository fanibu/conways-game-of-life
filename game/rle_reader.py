import os

class RleReader:

    @staticmethod
    def read_all_rle_file_names():
        rle_file_names = {}
        structure_type_names = None
        counter = 0
        for root, dirs, files in os.walk("../res/rle"):
            if structure_type_names is None:
                structure_type_names = dirs
                continue

            structure_types = []
            for file in files:
                structure_types.append(file[:-4])
            rle_file_names[structure_type_names[counter]] = structure_types
            counter += 1
        return rle_file_names

    @staticmethod
    def get_structure_dimensions(structure_type: str):
        with open("../res/rle/" + structure_type + ".txt", "r") as file:
            dimensions = file.readline().split(",")
            return int(dimensions[0]), int(dimensions[1]) # columns, rows

    @staticmethod
    def setup_gamefield(structure_type: str, cell_grid: list, horizontal_cells_range: int, vertical_cells_range: int):
        structure_columns, structure_rows = RleReader.get_structure_dimensions(structure_type)
        column_offset = int((horizontal_cells_range - structure_columns) / 2)
        row_offset = int((vertical_cells_range - structure_rows) / 2)

        with open("../res/rle/" + structure_type + ".txt", "r") as file:
            file.readline() # structure dimensions not needed
            structure_data = file.read()

            row_count = 0
            column_count = 0
            multiplier = ""
            for char in structure_data:
                if bool(char.isalpha()):
                    if multiplier == "":
                        multiplier = 1
                    else:
                        multiplier = int(multiplier)

                    if char == "o":  # default state for a cell is dead
                        for i in range(multiplier):
                            cell_grid[row_offset + row_count][column_offset + column_count + i].next_round_alive = True
                    column_count += multiplier
                    multiplier = ""  # reset multiplier
                elif bool(char.isdigit()):
                    multiplier += char
                elif char == "$":
                    if multiplier == "":
                        row_count += 1
                    else:
                        row_count += int(multiplier)
                    column_count = 0
                    multiplier = ""
                elif char == "\n":
                    pass
                elif char == "!":  # end of file
                    return
                else:
                    raise Exception("File error, while setting up gamefield, unknown character")


dict = {"a": ["a", "b", "c"], "b": ["d", "e", "f"]}
for entry in dict.keys():
    print(entry)