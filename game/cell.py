import random


class Cell:
    def __init__(self, size: int, x: int, y: int, random_probability: int, alive: bool = False):
        self.size = size
        self.x = x
        self.y = y

        self.draw_x1 = x * size + 1
        self.draw_y1 = y * size + 1

        self.alive = False
        self.neighbor_cells = []

        if random_probability >= 0:
            rand_nr = random.randrange(0, 100)
            if rand_nr < random_probability:
                self.next_round_alive = True
            else:
                self.next_round_alive = False
        else:
            self.next_round_alive = alive
        self.neighbor_cells = []

    def update_alive(self):
        living_count = 0
        for cell in self.neighbor_cells:
            living_count += cell.alive  # if cell lives, then state is 'True' and represents a '1'

        if self.alive and living_count < 2 or self.alive and living_count > 3:
            self.next_round_alive = False

        if not self.alive and living_count == 3:
            self.next_round_alive = True

    def find_neighbor_cells(self, mirrorfield: bool, cell_grid: list,
                            horizontal_cells_range: int, vertical_cells_range: int):
        for y in range(self.y - 1, self.y + 2):  # +2 because the stop-value is exclusive.
            for x in range(self.x - 1, self.x + 2):
                if x == self.x and y == self.y:
                    continue

                if mirrorfield:
                    if x < 0:
                        x = horizontal_cells_range - 1
                    elif x == horizontal_cells_range:
                        x = 0
                    if y < 0:
                        y = vertical_cells_range - 1
                    elif y == vertical_cells_range:
                        y = 0

                    self.neighbor_cells.append(cell_grid[y][x])
                else:
                    if x < 0 or y < 0 or x >= horizontal_cells_range or y >= vertical_cells_range:
                        self.neighbor_cells.append(Cell(self.size, x, y, random_probability=-1, alive=False))
                    else:
                        self.neighbor_cells.append(cell_grid[y][x])
